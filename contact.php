<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg bg-dark mb-5" data-bs-theme="dark" id="navbar">
        <div class="container">
          <a class="navbar-brand" href="index.html"><img src="images/Black and White Vintage Gym and Fitness Logo.png" alt="" class="logo"></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pricing.php">Pricing</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#">Contact</a>
              </li>
            </ul>
            <a href="#"><i class="fa-brands fa-instagram text-white me-3"></i></a>
            <a href="#"><i class="fa-brands fa-twitter text-white me-3"></i></a>
            <a href="#"><i class="fa-brands fa-facebook text-white"></i></a>
          </div>
        </div>
      </nav>
      <div class="container text-center">
            <h1>LET'S CHAT</h1>
            <h3>We would love to hear from you and share your thoughts with us</h3>
            <h4>We would be happy to answer all of your questions</h4>
            <p class="mb-5">Always and everywhere Bobby's gym and fitness.</p>
      </div>
        <div class="container d-flex">
            <div class="col text-center">
                <i class="fa-solid fa-mobile-screen-button"></i>
                <div>Call us at</div>
                <p>+1 234 4543 32</p>
            </div>
            <div class="col text-center">
                <i class="fa-solid fa-location-dot"></i>
                <div>Find us at</div>
                <p>123 Example.St Miami, Florida, USA</p>
            </div>
            <div class="col text-center">
                <i class="fa-solid fa-envelope"></i>
                <div>Email us at</div>
                <p>gym@example.com</p>
            </div>
        </div>
        <div>
            
        </div>
      <footer class="fixed-footer">
        <div class="container">
            <hr>
            <div class="row text-center my-5">
                <span>Copyright ©2023 All rights reserved | This template is made with <i class="fa-solid fa-heart"></i> by Bobby</span>
            </div>
        </div>
    </footer>
      <script src="https://kit.fontawesome.com/5029c06cab.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</body>
</html>
<?php

require_once 'Database.php';


class Dashboard extends Database {


    public function getVipPayments(){

        $sql = 'SELECT * FROM payments WHERE membership_id = 3';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetchAll();
        return count($row);
    }

    public function getAllPayments(){
        $sql = 'SELECT * FROM payments';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetchAll();
        return count($row);
    }

    public function newsletter(){
        $sql = 'SELECT * FROM users';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetchAll();
        return $row;
    }

    public function earnedMoney(){
        $sql = 'SELECT * FROM payments INNER JOIN membership WHERE membership_id = membership.id';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $sum = 0;
        foreach ($row as $data) {
            $sum += $data['price'];
        }
        return $sum;
    }

    public function adminAccess(){
        $sql = 'SELECT * FROM `admin`';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetchAll();
        return count($row);
    }
}
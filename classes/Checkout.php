<?php

require_once 'Database.php';

class Checkout extends Database {


    public function buyNow($address, $country, $zip, $cardName, $cardNumber, $cvv, $expiration, $membership_id, $city){
        $sql = 'INSERT INTO payments (`address`, country, zip, card_name, card_number, cvv, expiration, membership_id, city) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $stmt= $this->connect()->prepare($sql);
        $stmt->execute([$address, $country, $zip, $cardName, $cardNumber, $cvv, $expiration, $membership_id, $city]);
        header('location: ../pricing.php?success=Thanks for buying');
        die();
    }
}

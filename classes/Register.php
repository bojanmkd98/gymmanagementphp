<?php
require_once 'Database.php';

class Register extends Database {

    public function register($email){
        $sql = 'INSERT INTO `users` (email) VALUES (?)';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$email]);

    }

    public function checkEmail($email)
    {
        $sql = 'SELECT * FROM users WHERE email = ?';
        $stmt = $this->connect()->prepare($sql);

        if (!$stmt->execute([$email])) {
            $stmt = null;
            header('location: index.php?stmt=failed');
            die();
        }
        $resultCheck = true;
        if ($stmt->rowCount() > 0) {
            $resultCheck = false;
        } else {
            $resultCheck = true;
        }
        return $resultCheck;
    }


}
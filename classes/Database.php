<?php

    abstract class Database {

        private $host = 'localhost';
        private $dbname = 'gym';
        private $username = 'root';
        private $password = 'bojan123';


        public function connect(){
            try {
                $dsn = 'mysql:host='. $this->host . ';dbname='. $this->dbname;
                $pdo = new PDO($dsn, $this->username, $this->password);
                return $pdo;
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }
    }
?>
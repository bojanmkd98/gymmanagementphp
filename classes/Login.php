<?php
require_once 'Admin.php';

class Login extends Admin {

    private $username;
    private $password;

    public function  __construct($username, $password){
        $this->username = $username;
        $this->password = $password;
    }

    public function login(){
        if($this->checkInputs() == false){
            header('location: login.php?error=Please fill all the inputs');
            die();
        }
        $this->getAdmin($this->username, $this->password);
    }

    public function checkInputs(){
        $result = true;
        if(empty($this->username) || empty($this->password)){
            $result = false;
        }
        return $result;
    }
}
<?php
require_once 'Database.php';

class Membership extends Database {

    public function getMembership(){
        $sql = 'SELECT * FROM membership';
        $stmt = $this->connect()->query($sql);
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $cards) {
            echo '
                <div class="card p-0" style="width: 18rem;">
                <img src="../images/'.$cards['img'].'" class="card-img-top" alt="..." style="width: 18rem;">
                <div class="card-body text-center">
                <h2 class="card-text">$'.$cards['price'].' /month</h2>
                <h5 class="card-title">'.$cards['title'].'</h5>
                <a href="./checkout/checkout.php?id='.$cards['id'].'" class="btn btn-primary">Buy Now</a>
                </div>
            </div>
            ';
        }
    }

    public function buyMembership($id){
        $sql = 'SELECT * FROM membership WHERE id = ?';
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        echo '
        <ul class="list-group mb-3">
        <li class="list-group-item d-flex justify-content-between lh-sm">
          <div>
            <h6 class="my-0">Product name</h6>
            <small class="text-body-secondary">'.$row['title'].'</small>
          </div>
          <span class="text-body-secondary">$'.$row['price'].'</span>
        </li>
        <li class="list-group-item d-flex justify-content-between">
          <span>Total (USD)</span>
          <strong>$'.$row['price'].'</strong>
        </li>
      </ul>
        ';
    }

}

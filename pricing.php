<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg bg-dark mb-5" data-bs-theme="dark" id="navbar">
        <div class="container">
          <a class="navbar-brand" href="index.html"><img src="images/Black and White Vintage Gym and Fitness Logo.png" alt="" class="logo"></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#">Pricing</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact</a>
              </li>
            </ul>
            <a href="#"><i class="fa-brands fa-instagram text-white me-3"></i></a>
            <a href="#"><i class="fa-brands fa-twitter text-white me-3"></i></a>
            <a href="#"><i class="fa-brands fa-facebook text-white"></i></a>
          </div>
        </div>
      </nav>
      <?php if (isset($_GET['success']))
          echo '<div class="alert alert-success my-3 mx-auto col-2 text-center" role="alert">' . $_GET['success'] . '</div>'
        ?>
      <div class="pricing-header p-3 pb-md-4 mx-auto text-center col-4 mb-5">
        <h1 class="display-4 fw-normal">Pricing</h1>
        <p class="fs-5 text-body-secondary">Choose your best option for training</p>
      </div>
      <div class="container">
        <div class="row justify-content-evenly">
          <?php 
              require_once './classes/Membership.php';
              $membership = new Membership();
              $membership->getMembership();
            ?>
        </div>
      </div>
      <h1 class="text-center my-5 ">Compare Plans</h1>
      <div class="container mb-5">
        <table class="table">
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col">Normal</th>
              <th scope="col">Premium</th>
              <th scope="col">VIP</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Coach</th>
              <td><i class="fa-solid fa-check"></i></td>
              <td><i class="fa-solid fa-check"></i></td>
              <td><i class="fa-solid fa-check"></i></td>
            </tr>
            <tr>
              <th scope="row">Free Protein Shakes</th>
              <td></td>
              <td><i class="fa-solid fa-check"></i></td>
              <td><i class="fa-solid fa-check"></i></td>
            </tr>
            <tr>
              <th scope="row">FitKit Benefit</th>
              <td></td>
              <td><i class="fa-solid fa-check"></i></td>
              <td><i class="fa-solid fa-check"></i></td>
            </tr>
            <tr>
              <th scope="row">Pilates</th>
              <td></td>
              <td></td>
              <td><i class="fa-solid fa-check"></i></td>
            </tr>
            <tr>
              <th scope="row">Yoga</th>
              <td></td>
              <td></td>
              <td><i class="fa-solid fa-check"></i></td>
            </tr>
          </tbody>
        </table>
      </div>
      
      <footer>
        <div class="container">
            <div class="row mt-5 justify-content-center ">
                <div class="col">
                    <div class="mb-3 fw-bold">About Us</div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo, dolorum? Voluptatum eligendi aliquam ab nihil fuga voluptate excepturi esse in.</p>
                </div>
                <div class="col">
                    <div class="mb-3 fw-bold">Coach</div>
                    <div>Bojan Dimitrievski</div>
                </div>
                <div class="col">
                    <div class="mb-3 fw-bold">Have a question ?</div>
                    <div class="col">
                        <p>123 Example.St Miami, Florida, USA</p>
                        <p>+1 234 4543 32</p>
                        <p>gym@example.com</p>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3 fw-bold">Policy</div>
                    <div class="col">
                        <a href="#" class="text-dark text-decoration-none"><p>Terms & Conditions</p></a>
                        <a href="#" class="text-dark text-decoration-none"><p>Privacy Policy</p></a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row text-center my-5">
                <span>Copyright ©2023 All rights reserved | This template is made with <i class="fa-solid fa-heart"></i> by Bobby</span>
            </div>
        </div>
    </footer>

    <script src="https://kit.fontawesome.com/5029c06cab.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</body>
</html>
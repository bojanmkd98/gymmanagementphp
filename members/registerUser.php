<?php



if (isset($_POST['registerbtn'])) {
    $email = $_POST['email'];

    require_once 'registerController.php';
    $user = new RegisterController($email);
    $user->checkUser();
    header('location: registerForm.php?success=Thanks for joining us');
    die();
}

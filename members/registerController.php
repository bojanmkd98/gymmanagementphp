<?php
require_once '../classes/Register.php';

class RegisterController extends Register{

    private $email;


    public function __construct($email){
        $this->email = $email;
    }

    public function checkUser()
    {
        if ($this->emptyInput() == false) {
            echo json_encode(["msg" => "Empty input", "status" => 500]);
            header('location: registerForm.php?error=Please fill the input');
            die();
        }
        if ($this->invalidEmail() == false) {
            echo json_encode(["msg" => "Invalid email", "status" => 500]);
            header('location: registerForm.php?error=Invaild email');
            die();
        }
        if ($this->checkedEmail() == false) {
            echo json_encode(["msg" => "Email taken", "status" => 500]);
            header('location: registerForm.php?error=Email is already registered');
            die();
        }

        $this->register($this->email);
    }


    public function emptyInput()
    {
        $result = true;

        if (empty($this->email)) {
            $result = false;
        }
        return $result;
    }

    public function invalidEmail()
    {
        $result = true;

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $result = false;
        }

        return $result;
    }

    public function checkedEmail()
    {
        $result = true;
        if ($this->checkEmail($this->email) == false) {
            $result = false;
        }

        return $result;
    }
}
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Newsletter</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  </head>
  <body>
    <div class="container-fluid bg">
        <nav class="navbar navbar-expand-lg" data-bs-theme="dark" id="navbar">
        <div class="container">
          <a class="navbar-brand" href="../index.html"><img src="../images/Black and White Vintage Gym and Fitness Logo.png" alt="" class="logo"></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="../index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../about.php">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../pricing.php">Pricing</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../contact.php">Contact</a>
              </li>
            </ul>
            <a href="#"><i class="fa-brands fa-instagram text-white me-3"></i></a>
            <a href="#"><i class="fa-brands fa-twitter text-white me-3"></i></a>
            <a href="#"><i class="fa-brands fa-facebook text-white"></i></a>
          </div>
        </div>
      </nav>
      <?php if (isset($_GET['error']))
          echo '<div class="alert alert-danger my-3 mx-auto col-3 text-center" role="alert">' . $_GET['error'] . '</div>'
        ?>
        <?php if (isset($_GET['success']))
          echo '<div class="alert alert-success my-3 mx-auto col-3 text-center" role="alert">' . $_GET['success'] . '</div>'
        ?>
      <div class="card mx-auto col-4">
            <div class="card-body text-center">
                <h2>Join Our Newsletter</h2>
                <h4>Get exclusive content and Expert advice.</h4>
                <form action="registerUser.php" method="post">
                    <div class="input-group mb-3 w-50 mx-auto">
                        <input type="text" class="form-control" placeholder="Enter email" name="email">
                    </div>
                    <button class="btn btn-primary" name="registerbtn">Register</button>
                </form>
                <small class="card-text d-block my-3">Subscribe to our newsletter and stay updated.</small>
            </div>
        </div>
    <script src="https://kit.fontawesome.com/5029c06cab.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>
    <script src="main.js"></script>
  </body>
</html>
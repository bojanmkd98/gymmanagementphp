<?php

require_once './classes/Database.php';

class Cache extends Database{

    public function cache(){
        $cache_file = './cache/index.cache.php';

        if (file_exists($cache_file)) {
            include($cache_file);
        }else {
            $sql = 'SELECT * FROM images';
            $stmt = $this->connect()->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $str = "<div class= 'container d-flex justify-content-evenly mb-5'>";
            foreach($data as $img){
                $str.= '<img src="../images/'.$img['url'].'" alt="" style="width: 24rem" height="300px">';
            }
            $str.="</div>";
            $handle = fopen($cache_file, 'w');
            fwrite($handle, $str);
            fclose($handle);
            echo $str;
        }
    }
}

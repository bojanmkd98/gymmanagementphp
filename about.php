<?php
  require_once 'Cache.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg bg-dark mb-5" data-bs-theme="dark" id="navbar">
        <div class="container">
          <a class="navbar-brand" href="index.html"><img src="images/Black and White Vintage Gym and Fitness Logo.png" alt="" class="logo"></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#">About</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="pricing.php">Pricing</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact</a>
              </li>
            </ul>
            <a href="#"><i class="fa-brands fa-instagram text-white me-3"></i></a>
            <a href="#"><i class="fa-brands fa-twitter text-white me-3"></i></a>
            <a href="#"><i class="fa-brands fa-facebook text-white"></i></a>
          </div>
        </div>
      </nav>
      <div class="container text-center">
            <h1 class="mb-4">About us</h1>
            <p class="mb-4">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolorem a aliquid est exercitationem totam? Molestias eveniet repellat facere sapiente, deserunt nulla earum neque officia numquam reprehenderit accusantium eius delectus provident omnis? Eaque inventore dolorem nam porro amet at, laboriosam, excepturi optio ea, delectus eos repudiandae recusandae ipsum doloribus reprehenderit impedit. Ab dolor quia commodi quo molestias. Dolor praesentium asperiores iusto non odio nobis ab eos culpa dignissimos alias est consequatur ad hic officiis, magni, soluta dicta repellat? Laudantium nostrum mollitia ratione tempore perspiciatis earum omnis voluptates, animi sapiente repellendus deserunt nulla. Eum minima quos nisi! Dolores at voluptas amet. Praesentium doloremque asperiores explicabo voluptas autem repellat nihil in cupiditate expedita facere maxime sequi ab nesciunt hic quam consequatur ipsa fugiat consequuntur aliquid rerum, adipisci, maiores quidem vel nisi? Amet quaerat expedita dolorum minima molestiae pariatur veniam ratione nam, quis dolore officiis praesentium quisquam laborum eos itaque voluptas porro nobis fuga. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Magni officia cum architecto incidunt amet quidem neque voluptas. Dicta voluptates aperiam voluptatem accusamus voluptas doloremque assumenda quidem repellendus delectus. Nihil rem, itaque dicta quasi similique est esse et voluptate minima suscipit natus neque distinctio reprehenderit voluptatem, mollitia velit? Enim doloremque nihil eos, velit atque at. Eligendi nostrum aliquid earum facere odio, aut sunt dolorum? Ex, perspiciatis ducimus molestiae repellendus dolor dolores, a ratione at sed aspernatur ad natus earum qui velit labore, eligendi ut. Suscipit dolorem eveniet expedita quod, animi, excepturi in cumque quidem alias inventore iure laborum dolores atque sequi ratione velit officia itaque earum omnis rem possimus voluptatem. Animi tenetur magni, quos molestias libero tempora fugit neque. Deserunt itaque quasi necessitatibus ea a maiores facilis voluptas error omnis blanditiis nisi adipisci sint saepe aliquid obcaecati ipsum cum laboriosam, eaque repellat! Quisquam, rerum repellendus! Ratione eaque ipsum magni similique ipsam neque placeat deserunt. Repellendus ducimus explicabo voluptas? Eos commodi eius cum reprehenderit in accusantium dolores! Provident, fugiat. Natus, excepturi libero. Ullam ea temporibus id reiciendis recusandae aperiam odio voluptatibus inventore at! Corporis accusantium accusamus, explicabo culpa similique facere vitae iusto commodi ratione suscipit praesentium. Odit eveniet dolor magni non nisi!</p>
      </div>
      <div class="container-fluid bg-dark text-center text-light pb-5">
            <h2 class="py-4">Gallery</h2>
            <?php 
              $cache = new Cache();
              $cache->cache();
            ?>
      </div>
      <footer>
        <div class="container">
            <div class="row mt-5 justify-content-center ">
                <div class="col">
                    <div class="mb-3 fw-bold">About Us</div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo, dolorum? Voluptatum eligendi aliquam ab nihil fuga voluptate excepturi esse in.</p>
                </div>
                <div class="col">
                    <div class="mb-3 fw-bold">Coach</div>
                    <div>Bojan Dimitrievski</div>
                </div>
                <div class="col">
                    <div class="mb-3 fw-bold">Have a question ?</div>
                    <div class="col">
                        <p>123 Example.St Miami, Florida, USA</p>
                        <p>+1 234 4543 32</p>
                        <p>gym@example.com</p>
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3 fw-bold">Policy</div>
                    <div class="col">
                        <a href="#" class="text-dark text-decoration-none"><p>Terms & Conditions</p></a>
                        <a href="#" class="text-dark text-decoration-none"><p>Privacy Policy</p></a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row text-center my-5">
                <span>Copyright ©2023 All rights reserved | This template is made with <i class="fa-solid fa-heart"></i> by Bobby</span>
            </div>
        </div>
    </footer>
      <script src="https://kit.fontawesome.com/5029c06cab.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</body>
</html>
<?php
require_once '../classes/Dashboard.php';
$dashboard = new Dashboard();
session_start();
if (!isset($_SESSION['username'])) {
    header('location:login.php');
    die();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid px-0 d-flex">
        <div class="d-flex flex-column flex-shrink-0 p-3 text-white bg-dark col-2" style="height:100vh;">
            <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
            <img src="/images/Black and White Vintage Gym and Fitness Logo.png" alt="" width="32" height="32" class="me-3">
            <span class="fs-4">Bobby's Gym</span>
            </a>
            <hr>
            <ul class="nav nav-pills flex-column mb-auto">
            <li class="nav-item">
                <a href="#" class="nav-link active" aria-current="page">
                <svg class="bi me-2 text-light" width="16" height="16"><use xlink:href="#home"></use></svg>
                Home
                </a>
            </li>
            </ul>
            <hr>
            <a href="logout.php" class="text-decoration-none text-white"><button class="btn btn-danger w-100">Log Out</button></a>
        </div>
        <div class="dashboard px-4 mt-4 col-8">
            <div class="mb-3">
                <h2 class="fs-bold">Welcome, Bobby !</h2>
                <small><?php echo date("d l Y")?></small>
            </div>
            <div class="row flex-row my-5 justify-content-evenly">
                <div class="card col-3">
                    <div class="p-3 text-center">
                        <div class="mx-auto w-25"><svg class="svg-inline--fa fa-user-graduate fa-w-14 fs-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user-graduate" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M319.4 320.6L224 416l-95.4-95.4C57.1 323.7 0 382.2 0 454.4v9.6c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-9.6c0-72.2-57.1-130.7-128.6-133.8zM13.6 79.8l6.4 1.5v58.4c-7 4.2-12 11.5-12 20.3 0 8.4 4.6 15.4 11.1 19.7L3.5 242c-1.7 6.9 2.1 14 7.6 14h41.8c5.5 0 9.3-7.1 7.6-14l-15.6-62.3C51.4 175.4 56 168.4 56 160c0-8.8-5-16.1-12-20.3V87.1l66 15.9c-8.6 17.2-14 36.4-14 57 0 70.7 57.3 128 128 128s128-57.3 128-128c0-20.6-5.3-39.8-14-57l96.3-23.2c18.2-4.4 18.2-27.1 0-31.5l-190.4-46c-13-3.1-26.7-3.1-39.7 0L13.6 48.2c-18.1 4.4-18.1 27.2 0 31.6z"></path></svg></div>
                        <h4 class="my-1 font-sans-serif"><span class="text-700 mx-2" data-countup="{&quot;endValue&quot;:&quot;4968&quot;}"><?php echo count($dashboard->newsletter())?></span><span class="fw-normal text-600">Newsletter</span></h4>
                    </div>
                </div>
                <div class="card col-3">
                    <div class="p-3 text-center">
                        <div class="w-25 mx-auto mt-3"><svg class="svg-inline--fa fa-chalkboard-teacher fa-w-20 fs-2 text-info" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="chalkboard-teacher" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" data-fa-i2svg=""><path fill="currentColor" d="M208 352c-2.39 0-4.78.35-7.06 1.09C187.98 357.3 174.35 360 160 360c-14.35 0-27.98-2.7-40.95-6.91-2.28-.74-4.66-1.09-7.05-1.09C49.94 352-.33 402.48 0 464.62.14 490.88 21.73 512 48 512h224c26.27 0 47.86-21.12 48-47.38.33-62.14-49.94-112.62-112-112.62zm-48-32c53.02 0 96-42.98 96-96s-42.98-96-96-96-96 42.98-96 96 42.98 96 96 96zM592 0H208c-26.47 0-48 22.25-48 49.59V96c23.42 0 45.1 6.78 64 17.8V64h352v288h-64v-64H384v64h-76.24c19.1 16.69 33.12 38.73 39.69 64H592c26.47 0 48-22.25 48-49.59V49.59C640 22.25 618.47 0 592 0z"></path></svg></div>
                        <h4 class="mb-1 font-sans-serif"><span class="text-700 mx-2" data-countup="{&quot;endValue&quot;:&quot;4968&quot;}"><?php echo $dashboard->getAllPayments()?></span><span class="fw-normal text-600">New Members</span></h4>
                    </div>
                </div>
                <div class="card col-3">
                    <div class="p-3 text-center">
                        <div class="w-25 mx-auto mt-3"><svg class="svg-inline--fa fa-book-open fa-w-18 fs-2 text-success" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="book-open" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M542.22 32.05c-54.8 3.11-163.72 14.43-230.96 55.59-4.64 2.84-7.27 7.89-7.27 13.17v363.87c0 11.55 12.63 18.85 23.28 13.49 69.18-34.82 169.23-44.32 218.7-46.92 16.89-.89 30.02-14.43 30.02-30.66V62.75c.01-17.71-15.35-31.74-33.77-30.7zM264.73 87.64C197.5 46.48 88.58 35.17 33.78 32.05 15.36 31.01 0 45.04 0 62.75V400.6c0 16.24 13.13 29.78 30.02 30.66 49.49 2.6 149.59 12.11 218.77 46.95 10.62 5.35 23.21-1.94 23.21-13.46V100.63c0-5.29-2.62-10.14-7.27-12.99z"></path></svg></div>
                        <h4 class="mb-1 font-sans-serif"><span class="text-700 mx-2" data-countup="{&quot;endValue&quot;:&quot;4968&quot;}"><?php echo $dashboard->getVipPayments() ?></span><span class="fw-normal text-600">New VIP's</span></h4>
                    </div>
                </div>
            </div>
            <div class="card py-3 mb-5">
                    <div class="card-body py-3">
                        <div class="row g-0">
                            <div class="col-6 col-md-4 border-200 border-end pb-4">
                            <h6 class="pb-1 text-700">Admin access </h6>
                            <p class="font-sans-serif lh-1 mb-1 fs-2"><?php echo $dashboard->adminAccess()?></p>
                            </div>
                            <div class="col-6 col-md-4 border-200 border-end border-md-200 border-md-end pb-4 ps-3">
                            <h6 class="pb-1 text-700">Subscription sold </h6>
                            <p class="font-sans-serif lh-1 mb-1 fs-2"><?php echo $dashboard->getAllPayments() ?></p>
                            </div>
                            <div class="col-6 col-md-4 border-200 border-md-end-0 pb-4 pt-4 pt-md-0 ps-md-3">
                            <h6 class="pb-1 text-700">Earned</h6>
                            <p class="font-sans-serif lh-1 mb-1 fs-2">$<?php echo $dashboard->earnedMoney() ?> </p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="d-flex flex-column align-items-center">
                <table class="table table-bordered rounded">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td >Larry the Bird</td>
                        <td>Birds</td>
                        <td>@twitter</td>
                        </tr>
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="pt-4 col-2 px-4 bg-dark text-white">
            <div class="d-flex justify-content-between align-items-center">
                <h6>Profile</h3>
            </div>
            <hr>
            <div class="d-flex align-items-center justify-content-center my-5 flex-column">
                <img src="https://github.com/mdo.png" alt="" width="72" height="72" class="rounded-circle me-2">
                <div class="fw-bold my-2">Bojan Dimitrijevski</div>
                <small class="fw-lighter">Owner</small>
            </div>
    </div>
    <script src="https://kit.fontawesome.com/5029c06cab.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/dashboard.js"></script>
</body>
</html>
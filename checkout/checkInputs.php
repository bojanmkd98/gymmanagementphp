<?php
require_once 'checkoutController.php';

if(isset($_POST['checkoutBtn'])){
    $address = $_POST['address'];
    $country = $_POST['country'];
    $city = $_POST['city'];
    $zip = $_POST['zip'];
    $cardName = $_POST['card_name'];
    $cardNumber = $_POST['card_number'];
    $expiration = $_POST['expiration'];
    $cvv = $_POST['cvv'];
    $membership_id = $_POST['id'];
    intval($membership_id);
    $checkout = new CheckoutController($address, $country, $city, $zip, $cardName, $cardNumber, $expiration, $cvv, $membership_id);
    $checkout->checkout();

}
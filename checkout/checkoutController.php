<?php

require_once '../classes/Checkout.php';

class CheckoutController extends Checkout{
    private $address;
    private $country;
    private $city;
    private $zip;
    private $cardName;
    private $cardNumber;
    private $expiration;
    private $cvv;
    private $membership_id;

    public function __construct($address, $country, $city, $zip, $cardName, $cardNumber, $expiration, $cvv, $membership_id){
        $this->address = $address;
        $this->country = $country;
        $this->city = $city;
        $this->zip = $zip;
        $this->cardName = $cardName;
        $this->cardNumber = $cardNumber;
        $this->expiration =$expiration;
        $this->cvv = $cvv;
        $this->membership_id = $membership_id;
    }
    public function checkout(){
        if($this->checkInputs() == false){
            header("location: checkout.php?id=". $this->membership_id."&error=Please fill all the inputs");
            die();
        }
        if ($this->checkNumeric() == false) {
            header("location: checkout.php?id=". $this->membership_id."&error=CVV, Zip and Card Number must be numeric");
            die();
        }

        $payment = new Checkout();
        $payment->buyNow($this->address, $this->country,  $this->zip, $this->cardName, $this->cardNumber, $this->cvv, $this->expiration, $this->membership_id, $this->city);
    }

    public function checkInputs(){
        $result = true;
        if(empty($this->address) || empty($this->country) || empty($this->city) || empty($this->zip) || empty($this->cardName) || empty($this->cardNumber) || empty($this->expiration) || empty($this->cvv)){
            $result = false;
        }
        return $result;
    }

    public function checkNumeric(){
        $result = true;
        if(!is_numeric($this->zip) || !is_numeric($this->cardNumber) || !is_numeric($this->cvv)){
            $result = false;
        }
        return $result;
    }
}